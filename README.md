## `Optional() 서비스단 예외 처리 프로젝트 :)`

### 1. API 설계
|URI|HTTP 메서드|설명|
|---|-----------|-----|
|`/api/library/book`|GET|도서 전체 조회|
|`/api/library/book?isbn=1919`|GET|도서 ISBN으로 도서 조회|
|`api/library/book/:id`|GET|도서 ID로 도서 조회|
|`api/library/book`|POST|도서 등록|
|`api/library/book/id`|DELETE|도서 삭제|
|`api/library/book/lend`|POST|도서 대출|
|`api/library/member`|POST|회원 등록|
|`api/library/member/:id`|PATCH|회원 수정|





