package com.example.optional.model;

public enum MemberStatus {
    ACTIVE, DEACTIVATED
}
